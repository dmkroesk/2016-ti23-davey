#ifndef ALARM_INC
#define ALARM_INC
#include "time.h"

#define MAX_ALARMS 5	//The max amount of alarms that should be stored and checked etc.

typedef struct
{
	int valid;	//Used to check if alarm in memory is valid and not random data
	int id;	//The id on the website, to allow updating
	int updated;	//Mark alarm for deletion
	int on;
	char name[16]; //given name of the alarm
	int hour; //time when alarm has to ring
	int min; 
	u_long ip; //ip of stream that will be played when alarm rings 
	u_short port; //port of stream
	u_char path[64]; //path to stream
	int snooze; //times the alarm has been snoozed
} ALARM_STRUCT;

int compareAlarmTime(ALARM_STRUCT alarm, tm currentTime);
void tweetAlarm(int hour, int min, int sec);
void createAlarm(int id, char* name, int hour, int min, u_long ip, u_short port, u_char *path);
int addAlarm(ALARM_STRUCT * alarm);
void clearAlarms(void);
void checkAlarms(void);

void GetAlarmName(int, char *);
void GetAlarmTime(int, char*);
int GetNumAlarms(void);
int GetAlarmOn(int);
void ToggleAlarmOn(int);

void InitAlarms(void);
void GetCurrentAlarmName(char *);
int AlarmRinging(void);
int AlarmsOn(void);
void StopAlarm(void);
void SnoozeAlarm(void);

ALARM_STRUCT GetAlarm(int);

#endif
