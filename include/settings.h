#ifndef _SETTINGS_DEF
#define _SETTINGS_DEF

//Save struct
struct _SAVE_BLOCK {
	char length;	//struct length
	int timeZone;	//timezone offset
	int firstTimeSetup; //bool FTS done
	int volume;	//volume level
	int treble;	//treble level
	int bass;	//bass level
	int balance;	//balance
	int backlight;	//bool backlight on	
	int snooze;	//snooze time
	int sync;	//sync time
	u_char dev0;	//Device ID char 1
	u_char dev1;	//Device ID char 2
	u_char dev2;	//Device ID char 3
	u_char dev3;	//Device ID char 4
	u_char dev4;	//Device ID char 5
	u_char dev5;	//Device ID char 6
};
//static struct _SAVE_BLOCK *pointerTest = &file;

void SaveSettings(void);
void LoadSettings(void);

void FactoryReset(void);

void SetFirstTimeSetup(int);
int GetFirstTimeSetup(void);
void SetTimeZone(int);
int GetTimeZone(void);
void SetVolume(int);
int GetVolume(void);
void SetTreble(int);
int GetTreble(void);
void SetBass(int);
int GetBass(void);
void SetBalance(int);
int GetBalance(void);
void SetBacklight(int);
int GetBacklight(void);
void SetSnooze(int);
int GetSnooze(void);
void SetSync(int);
int GetSync(void);

void SetDeviceID(char *);
void GetDeviceID(char *);

void Reboot(void);
int GetReboot(void);

struct _SAVE_BLOCK *getSettingsFile(void);	

#endif
