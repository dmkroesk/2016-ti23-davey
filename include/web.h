#ifndef _Web_H
#define _Web_H

#define MAXTOKENS 512
#define LARGEALLOC 1024
#define SMALLALLOC 256

FILE *getStream(TCPSOCKET * sock, u_long ip, u_short port);
void getJSON(FILE * stream, char * path, char * result);
void parseAlarmJSON(char * json);
int getAlarms(void);
int resetServer(void);
int tweet(char *);
int getStreams(void);
void parseStreamJSON(char *json);
int getSchedules(void);
void parseScheduleJSON(char *json);
int registerDevice(void);
int getTimezoneOffset(int * timezone);
void parseTimeZoneJSON(char * json, int * timezone);
void parseRegisterJSON(char * json);
#endif
