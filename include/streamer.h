#ifndef STREAMER_INC
#define STREAMER_INC

#define MAX_HEADERLINE 512
#define MAX_STREAMS 5

typedef struct
{
	int valid;
	int id;
	int updated;
	char name[16]; //given name of the alarm
	u_long ip; //ip of stream that will be played when alarm rings 
	u_short port; //port of stream
	u_char path[64]; //path to stream
} STREAM_STRUCT;

extern FILE *ConnectStation(TCPSOCKET *sock, u_long ip, u_short port, u_char *path, u_long *metaint);

void createStream(int id, char* name, u_long ip, u_short port, u_char *path);
int addStream(STREAM_STRUCT * stream);
void clearStreams(void);

void InitStreams(void);

void GetStreamName(int, char *);
int GetNumStreams(void);
void PlayStream(int);

STREAM_STRUCT GetStream(int);

#endif
