#ifndef SCHEDULER_H
#define SCHEDULER_H
#include "time.h"

#define MAX_SCHEDULES 5

typedef struct
{
	int valid;
	int id;
	int updated;
	int on;
	char name[16]; //given name of the schedule
	int hour; //time when schedule has to start
	int min; 
	int dhour; //duration of schedule
	int dmin;
	u_long ip; //ip of stream that will be played when schedule starts 
	u_short port; //port of stream
	u_char path[64]; //path to stream
} SCHEDULE_STRUCT;

int compareScheduleTime(SCHEDULE_STRUCT schedule, tm currentTime);
void tweetSchedule(int hour, int min, int sec);
void createSchedule(int id, char* name, int hour, int min, int dhour, int dmin, u_long ip, u_short port, u_char *path);
int addSchedule(SCHEDULE_STRUCT * alarm);
void clearSchedules(void);
void checkSchedules(void);

void GetScheduleName(int, char *);
void GetScheduleTime(int, char*);
int GetNumSchedules(void);
int GetScheduleOn(int);
void ToggleScheduleOn(int);

void InitSchedules(void);
void GetCurrentScheduleName(char *);
int ScheduleRinging(void);
int SchedulesOn(void);
void StopSchedule(void);

SCHEDULE_STRUCT GetSchedule(int);

#endif
