<?php
	header('Content-Type: application/json');
	require('../inc/connect.php');
	
	// Twitter account login en email login:
	// twitter username: avansp23
	// ww twitter: xxxx
	// email: xxxx
	// ww email: xxxx
	// tweets via https://ifttt.com/
	
	//Declare get variables
	$body = $_GET["body"];
	$id = $_GET["id"];
	
	error_log("Page loaded: $body for device $id\n", 3, "lipa.log");	

	//If a get variable is empty or missing
	if($body == "" || $id == "")
	{
		echo '{"error":"body and/or device id not set"}'; 
		error_log("body/dev not set\n", 3, "lipa.log");
		exit;
	}

	//Check if device id exists
	$valid = 0;
	$sql = "SELECT `id` FROM `device` WHERE dev_id='" . $id . "'";		
	$query = ExecQuery($mysqli, $sql);	
	
	if($query->num_rows < 1)
	{
		echo '{"error":"device id does not exist"}'; 
		error_log("Device id does not exist\n", 3, "lipa.log");
		exit;
	}

	$to      = 'trigger@recipe.ifttt.com';
	$subject = '#send';
	$headers = 'From: avanstwitter@gmail.com' . "\r\n";
	$mailbody = $body . " - #" . $id;
	//If body is too long (over 140 char's), only send first 140 characters
	if(strlen($mailbody) > 140){
		$mailbody = substr($mailbody, 0, 140);
	}
	
	mail($to, $subject, $mailbody, $headers);

	error_log("Email sent successfully: $mailbody\n", 3, "lipa.log");
	echo '{"success":"true"}';
?>