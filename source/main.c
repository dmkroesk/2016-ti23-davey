/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */
 
 
 

#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <pro/dhcp.h>
#include <pro/sntp.h>
#include <dev/nicrtl.h>
#include <sys/socket.h>
#include <netinet/tcp.h>

#include <sys/confnet.h>

#include <dev/board.h>
#include <pro/httpd.h>
#include <pro/asp.h>
#include <pro/discover.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "vs10xx.h"
#include "rtc.h"
#include "network.h"

#include <time.h>
#include "rtc.h"

#include "player.h"
#include "settings.h"
#include "menu.h"
#include "alarm.h"
#include "web.h"
#include "scheduler.h"
#include "streamer.h"




/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
 * \addtogroup System
 */

/*@{*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/


/* ����������������������������������������������������������������������� */
/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{

    /*
     *  scan for valid keys AND check if a MMCard is inserted or removed
     */
    KbScan();
    CardCheckCard();
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Initialise Digital IO
 *  init inprintf to '0', outprintf to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port C:     Address bus
     */

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif //USE_JTAG

    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
    int nError = 0;

    if (OnOff==ON)
    {
        nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
        if (nError == 0)
        {
            init_8_bit_timer();
        }
    }
    else
    {
        // disable overflow interrupt
        disable_8_bit_timer_ovfl_int();
    }
}

void SyncWithWeb(void)
{
	LedControl(LED_OFF);
	
	NutSleep(50);
	
	LedControl(LED_ON);
	
	printf("[MAIN] Retrieving Alarms\n");
	//Get the alarms
	if(getAlarms())
	{
		LedControl(LED_OFF);
		
		NutSleep(50);
		
		LedControl(LED_ON);
		
		printf("[MAIN] Clearing Alarms\n");
		//Remove old alarms
		clearAlarms();
	}
	
	LedControl(LED_OFF);
	
	NutSleep(50);
	
	LedControl(LED_ON);
	
	printf("[MAIN] Retrieving Streams\n");
	//Get the streams
	if(getStreams())
	{
		LedControl(LED_OFF);
		
		NutSleep(50);
		
		LedControl(LED_ON);
		
		printf("[MAIN] Clearing Streams\n");
		//Remove old streams
		clearStreams();
	}
	
	LedControl(LED_OFF);
	
	NutSleep(50);
	
	LedControl(LED_ON);
	
	printf("[MAIN] Retrieving Schedules\n");
	//Get the schedules
	if(getSchedules())
	{
		LedControl(LED_OFF);
		
		NutSleep(50);
		
		LedControl(LED_ON);
		
		printf("[MAIN] Clearing Schedules\n");
		//Remove old schedules
		clearSchedules();
	}
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Main entry of the SIR firmware
 *
 * All the initialisations before entering the for(;;) loop are done BEFORE
 * the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
 * initialisatons need to be done again when leaving the Setup because new values
 * might be current now
 *
 * \return \b never returns
 */
/* ����������������������������������������������������������������������� */
int main(void)
{


    /*
     *  First disable the watchdog
     */
    WatchDogDisable();

    NutSleep(100);

    SysInitIO();
	
	SPIinit();

	//Init LED
	LedInit();
	
	//Init LCD Screen
	LcdLowLevelInit();

	//Init USB Communications
    Uart0DriverInit();
    Uart0DriverStart();
	
	printf("\n\n\n");
	printf("|----------------------------------PROGRAM START----------------------------------|");
	printf("\n\n");
	
	NutSleep(10);

	printf("[MAIN] HEAP - %d\n", (int) NutHeapAvailable());
	
	LcdBackLight(LCD_BACKLIGHT_ON);
	LedControl(LED_ON);
	
	NutSleep(50);
	
	LcdLine(0, "Connecting...");
	
	//Init Network connection
	networkInitialize();
	
	LcdLine(0, "Booting...");
	
	NutSleep(50);
	
	
	//Init audio chip
	VsPlayerInit();
	
	//Init logging module
	LogInit();

    CardInit();

	//Init RTC chip
    X12Init();
	
	//Load EEPROM Settings
	LoadSettings();
	
	//Restore audio settings
	initPlayer();
	
	//Init local flash storage
    if (At45dbInit()==AT45DB041B)
    {
        // ......
    }


    RcInit();
    
	KbInit();
	
	NutSleep(150);
	
	//Allocate alarms and streams and schedules space
	InitAlarms();
	InitStreams();
	InitSchedules();
	
	LedControl(LED_OFF);
	
	//Try to register the device on the website
	registerDevice();
	
	NutSleep(10);
	
	LedControl(LED_ON);
	
	//Check if time and web should be synced
	if(GetFirstTimeSetup())
	{
		SyncTime();
		LedControl(LED_ON);
		SyncWithWeb();
	}
	
	LedControl(LED_OFF);
	
	NutSleep(50);
	
	LedControl(LED_ON);
	
	LcdClear();
	
	//Start menu
	InitMenu();
	
    SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt

    /*
     * Increase our priority so we can feed the watchdog.
     */
    NutThreadSetPriority(2);

	/* Enable global interrupts */
	sei();

	printf("[MAIN] Boot complete\n");
	
	int ntp = 0;
	int webget = 0;
	int check = 0;
	
	for(;;)
	{
		if(GetReboot() != 0) //Check if device should reboot
		{
			printf("[MAIN] Rebooting...\n");
			break;
		}
		
		if(GetSync() == 0) //Check if web sync is enabled
		{
				//No sync
				webget = 0;
				ntp = 0;
		}
		else if(webget >= (2400 * GetSync())) //Sync every n minutes
		{
			printf("[MAIN] Starting webget\n");
			
			if(NutHeapAvailable() > 8192) //Only sync if enough heap is still available
				SyncWithWeb();
			else
				NutSleep(200);
			
			webget = 0;
			ntp++;
			
			if(GetSync() != 0 && (ntp >= 720 / GetSync() )) //NTP sync every 12 hours
			{
				printf("[MAIN] Starting NTP Sync\n");
				NutSleep(10);
				SyncTime();
				ntp = 0;
			}
		}
		
		if(check >= 400) //Every 10 seconds
		{
			NutSleep(10);
	
			printf("[MAIN] Checking Alarms\n");
			//Check all the alarms
			checkAlarms();
			
			printf("[MAIN] Checking Schedules\n");
			//Check all the schedules
			checkSchedules();
			
			check = 0;
		}
		
		if(check % 80 == 0) // Every 2 seconds show the current heap
		{
			printf("[MAIN] Device alive - %d\n", (int) NutHeapAvailable());
		}
		
		NutSleep(25);
		
		webget++;
		check++;
		
		WatchDogRestart();
	}
	
	//Device reached end of program. Should reboot automatically, but shows the message below just in case
	printf("[MAIN] End of program\n");
	printf("[MAIN] Powercycle required\n");
	
	LcdClear();
	LcdBackLight(LCD_BACKLIGHT_ON);
	LcdLine(0, "Powercycle");
	LcdLine(1, "required");
	
	NutSleep(150);

    return(1); 
}
/* ---------- end of module ------------------------------------------------ */

/*@}*/
