#include <dev/board.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <dev/nvmem.h>
#include <arpa/inet.h>
#include <sys/timer.h>
#include <sys/thread.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <string.h>

#include "time.h"
#include "scheduler.h"
#include "rtc.h"
#include "player.h"
#include "web.h"
#include "menu.h"

SCHEDULE_STRUCT *schedules;

int * ringingschedule;
int * scheduleringing;

void GetCurrentScheduleName(char * str)
{
	strcpy(str, schedules[*ringingschedule].name);
}

int ScheduleRinging(void)
{
	return *scheduleringing;
}

void StopSchedule(void)
{
	*scheduleringing = 0;
	*ringingschedule = -1;
	stop();
}

//alarm is the alarm to be compared with the currentTime
//currentTime is the currentTime, also set in the rtc chip
int compareScheduleTime(SCHEDULE_STRUCT schedule, tm currentTime)
{				
	int min = schedule.min;
	int hour = schedule.hour;
	
	int dmin = schedule.dmin;
	int dhour = schedule.dhour;
	
	dmin = min + dmin;
	dhour = dhour + hour;
	dhour = dhour + (dmin / 60);
	dhour = dhour % 24;
	dmin = dmin % 60;
				
	printf("[SCHEDULE] Comparing [%02d:%02d] ", currentTime.tm_hour, currentTime.tm_min);
	printf("to schedule [%02d:%02d] until [%02d:%02d]\n", hour, min, dhour, dmin);			
				
	if(hour == currentTime.tm_hour){
		
		if(min == currentTime.tm_min && *scheduleringing == 0){
			return 1; //time is same as start
		}
	}
	
	if(dhour == currentTime.tm_hour){
		
		if(dmin == currentTime.tm_min && *scheduleringing == 1){
			return 2; //time is same as stop
		}
	}
	
	return 0;//time is not same alarm
}

void tweetSchedule(int hour, int min, int sec)
{
	char str[141];
	
	sprintf(str, "Schedule%%20at%%20%02d:%02d:%02d", hour, min, sec);
	tweet(str);
}

void createSchedule(int id, char* name, int hour, int min, int dhour, int dmin, u_long ip, u_short port, u_char *path)
{
	SCHEDULE_STRUCT * schedule = (SCHEDULE_STRUCT *) calloc(1, sizeof(SCHEDULE_STRUCT));
	
	schedule->valid = 1;
	schedule->on = 1;
	schedule->id = id;
	schedule->updated = 1;
	
	strncpy(schedule->name, name, sizeof(schedule->name));
	schedule->hour = hour;
	schedule->min = min;
	schedule->dhour = dhour;
	schedule->dmin = dmin;
	
	schedule->ip = ip;
	schedule->port = port;
	strncpy((char*) schedule->path, (char*) path, sizeof(schedule->path));
	
	addSchedule(schedule);
}

void clearSchedules()
{
	int i;
	
	for(i = 0; i<MAX_SCHEDULES; i++)
	{
		if(schedules[i].updated == 0)
		{
			printf("[SCHEDULE] Removing old schedule %d with id %d\n", i, schedules[i].id);
			memset(&schedules[i], 0, sizeof(schedules[i]));
		}
		else
		{
			printf("[SCHEDULE] Keeping schedule %d with id %d\n", i, schedules[i].id);
			schedules[i].updated = 0;
		}
	}
}

int addSchedule(SCHEDULE_STRUCT * schedule)
{
		int i;
	
		for(i = 0; i<MAX_SCHEDULES; i++)
		{
		
			if(schedules[i].valid == 1 && schedules[i].id == schedule->id)
			{
				printf("[SCHEDULE] Updated schedule at pos %d\n", i);
				schedules[i].updated = 1;
				schedules[i].hour = schedule->hour;
				schedules[i].min = schedule->min;
				schedules[i].dhour = schedule->dhour;
				schedules[i].dmin = schedule->dmin;
				strncpy(schedules[i].name, schedule->name, sizeof(schedules[i].name));
				schedules[i].ip = schedule->ip;
				schedules[i].port = schedule->port;
				strncpy((char*) schedules[i].path, (char*) schedule->path, sizeof(schedules[i].path));
				break;
			}
			else if(schedules[i].valid != 1)
			{
				printf("[SCHEDULE] Adding new schedule to pos %d\n", i);
				schedules[i] = *schedule;
				break;
			}
		}
		
		free(schedule);
		
		return 0;
}

SCHEDULE_STRUCT GetSchedule(int i)
{
	if(i > MAX_SCHEDULES - 1)
	{
		i = MAX_SCHEDULES - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	return schedules[i];
}

void checkSchedules(void)
{
	int i;
	int p;
	
	for(i = 0; i<MAX_SCHEDULES; i++)
	{
		if(schedules[i].valid == 1 && schedules[i].on == 1)
		{
			p = compareScheduleTime(schedules[i], *GetTimeStruct());
			
			if(p == 1)
			{
				printf("[SCHEDULE] Schedule %d:%s is starting", i, schedules[i].name);
				
				//Start schedule
				*scheduleringing = 1;
				*ringingschedule = i;
				
				tweetSchedule(schedules[i].hour, schedules[i].min, 0);
				
				NutSleep(10);
				
				play(schedules[i].ip, schedules[i].port, schedules[i].path);
			}
			else if(p == 2)
			{
				printf("[SCHEDULE] Schedule %d:%s has timed out", i, schedules[i].name);
				StopSchedule();
			}
			else
			{
				//Do nothing
			}
		}
	}
}

void GetScheduleName(int i, char * str)
{
	if(i > MAX_SCHEDULES - 1)
	{
		i = MAX_SCHEDULES - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	strcpy(str, schedules[i].name);
}

void GetScheduleTime(int i, char* str)
{
	if(i > MAX_SCHEDULES - 1)
	{
		i = MAX_SCHEDULES - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	sprintf(str, "%02d:%02d-%02d:%02d", schedules[i].hour, schedules[i].min, schedules[i].dhour, schedules[i].dmin);
}

int GetScheduleOn(int i)
{	
	if(i > MAX_SCHEDULES - 1)
	{
		i = MAX_SCHEDULES - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	return schedules[i].on;
}

int GetNumSchedules(void)
{
	int count = 0;
	int i;
	for(i=0; i< MAX_SCHEDULES; i++)
	{
		if(schedules[i].valid == 1)
			count++;
	}
	
	return count;
}

void ToggleScheduleOn(int i)
{	
	if(i > MAX_SCHEDULES - 1)
	{
		i = MAX_SCHEDULES - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
		
	if(schedules[i].on == 1)
	{
		schedules[i].on = 0;
	}
	else
	{
		schedules[i].on = 1;
	}
}

int SchedulesOn(void)
{
	int on = 0;
	int i;
	
	for(i=0; i< MAX_SCHEDULES; i++)
	{
		if(schedules[i].valid == 1 && schedules[i].on == 1)
		{
			on = 1;
			break;
		}
	}
	
	return on;
}

void InitSchedules( void )
{
	scheduleringing = calloc(1, sizeof(int));
	ringingschedule = calloc(1, sizeof(int));
	
	*scheduleringing = 0;
	*ringingschedule = -1;
	
	schedules = calloc(MAX_SCHEDULES, sizeof(SCHEDULE_STRUCT));
}
