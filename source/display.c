/* ========================================================================
 * [PROJECT]    SIR100
 * [MODULE]     Display
 * [TITLE]      display source file
 * [FILE]       display.c
 * [VSN]        1.0
 * [CREATED]    26092003
 * [LASTCHNGD]  06102006
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 * [PURPOSE]    contains all interface- and low-level routines to
 *              control the LCD and write characters or strings (menu-items)
 * ======================================================================== */

#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
char customCharArray[64] = {
 0b00000,0b01000,0b11111,0b01000,0b00010,0b11111,0b00010,0b00000, //Web sync true
 0b00000,0b00100,0b01110,0b00000,0b00000,0b01110,0b00100,0b00000, //Up Down
 0b00100,0b01110,0b01110,0b01110,0b11111,0b00000,0b00100,0b00000, //Alarm Clock
 0b00000,0b00100,0b01101,0b11101,0b01101,0b00100,0b00000,0b00000, //Volume ON
 0b00000,0b00100,0b01100,0b11100,0b01100,0b00100,0b00000,0b00000, //Volume OFF
 0b00000,0b00001,0b00011,0b00111,0b01111,0b11111,0b00000,0b00000, //Network TRUE
 0b00000,0b01000,0b01100,0b01110,0b01100,0b01000,0b00000,0b00000, //PLAY
 0b00000,0b01110,0b10101,0b10111,0b10001,0b01110,0b00000,0b00000  //Time Sync TRUE
};

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void LcdWriteByte(u_char, u_char);
static void LcdWriteNibble(u_char, u_char);
static void LcdWaitBusy(void);

/*!
 * \addtogroup Display
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief control backlight
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
void LcdBackLight(u_char Mode)
{
    if (Mode==LCD_BACKLIGHT_ON)
    {
        sbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn on backlight
    }

    if (Mode==LCD_BACKLIGHT_OFF)
    {
        cbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn off backlight
    }
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Write a single character on the LCD
 *
 * Writes a single character on the LCD on the current cursor position
 *
 * \param LcdChar character to write
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
void LcdChar(char MyChar)
{
    LcdWriteByte(WRITE_DATA, MyChar);
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Clears the LCD
 *
 * Clears the data from the LDC and sets the position to the first position.
 *
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
void LcdClear()
{
    LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
    NutDelay(5);
	LcdWriteByte(WRITE_COMMAND, 0x80);			// DD-RAM address counter (cursor pos) to '0'
}

void LcdLine(u_char line, char text[])
{
	int i;
	
	if(line == 0)	//Top line
	{
		LcdWriteByte(WRITE_COMMAND, 0x80); // command to write to line 1	
		for(i = 0; i< 16; i++)
		{
			LcdChar(32);	//Clear the line
		}
		LcdWriteByte(WRITE_COMMAND, 0x80); // command to write to line 1
	}
	else if(line == 1)	//Bottom line
	{
		LcdWriteByte(WRITE_COMMAND, 0xC0); // command to write to line 2
		for(i = 0; i< 16; i++)
		{
			LcdChar(32);	//Clear the line
		}
		LcdWriteByte(WRITE_COMMAND, 0xC0); // command to write to line 1
	}
	else
	{
		return;
	}
	
	int count = 0;
	
	while(text[count] != '\0')
	{
		//Replace ~n with the decimal n
		//This displays the custom character
		if(text[count] == '~')
		{
			count++;
			char num = text[count];
			
			int number = num - '0';
			
			LcdWriteByte(WRITE_DATA, number);
		}
		else
		{
			//Write char to display
			LcdChar(text[count]);
		}
		
		count++;
		
		if(count > 24)
			break;
		
		NutSleep(1);
	}
}

void LcdScrollLine(u_char line, int pos, char text[])
{
	int i;
	
	if(line == 0)
	{
		LcdWriteByte(WRITE_COMMAND, 0x80); // command to write to line 1	
		for(i = 0; i< 16; i++)
		{
			LcdChar(32);
		}
		LcdWriteByte(WRITE_COMMAND, 0x80); // command to write to line 1
	}
	else if(line == 1)
	{
		LcdWriteByte(WRITE_COMMAND, 0xC0); // command to write to line 2
		for(i = 0; i< 16; i++)
		{
			LcdChar(32);
		}
		LcdWriteByte(WRITE_COMMAND, 0xC0); // command to write to line 1
	}
	else
	{
		return;
	}
	
	//Start at scroll location
	int count = pos;
	
	while(text[count] != '\0')
	{
		if(text[count] == '~')
		{
			count++;
			char num = text[count];
			
			int number = num - '0';
			
			LcdWriteByte(WRITE_DATA, number);
		}
		else
		{
			LcdChar(text[count]);
		}
		
		count++;
		
		if(count > 24 + pos)
			break;
		
		NutSleep(1);
	}
}

//Write data to the Custom Character RAM
void LcdCGRAM(char *icon, int length)
{
	//SELECT CGRAM
	LcdWriteByte(WRITE_COMMAND, 0x40);
	
	int count;
	
	//Write the array with custom icons
	for(count = 0; count < length; count++)
	{
		LcdChar(icon[count]);
		NutSleep(1);
	}
	
	//Select the display RAM
	LcdWriteByte(WRITE_COMMAND, 0x80);
}

void LcdSetPosition(int line)
{
	if(line)
	{
		LcdWriteByte(WRITE_COMMAND, 0xC0);
	}
	else
	{
		LcdWriteByte(WRITE_COMMAND, 0x80);
	}
}


/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Low-level initialisation function of the LCD-controller
 *
 * Initialise the controller and send the User-Defined Characters to CG-RAM
 * settings: 4-bit interface, cursor invisible and NOT blinking
 *           1 line dislay, 10 dots high characters
 *
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
 void LcdLowLevelInit()
{
    u_char i;

    NutSleep(140);                               // wait for more than 140 ms after Vdd rises to 2.7 V

    for (i=0; i<3; ++i)
    {
        LcdWriteNibble(WRITE_COMMAND, 0x33);     // function set: 8-bit mode; necessary to guarantee that
        NutDelay(4);                              				 // SIR starts up always in 5x10 dot mode
    }

    LcdWriteNibble(WRITE_COMMAND, 0x22);        // function set: 4-bit mode; necessary because KS0070 doesn't
    NutDelay(1);                                					// accept combined 4-bit mode & 5x10 dot mode programming

    //LcdWriteByte(WRITE_COMMAND, 0x24);        // function set: 4-bit mode, 5x10 dot mode, 1-line
    LcdWriteByte(WRITE_COMMAND, 0x28);          // function set: 4-bit mode, 5x7 dot mode, 2-lines
    NutDelay(5);

	LcdCGRAM(customCharArray, 64);
	NutDelay(5);
	
    LcdWriteByte(WRITE_COMMAND, 0x0C);          // display ON/OFF: display ON, cursor OFF, blink OFF
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
    NutDelay(5);

    LcdWriteByte(WRITE_COMMAND, 0x06);          // entry mode set: increment mode, entire shift OFF


    LcdWriteByte(WRITE_COMMAND, 0x80);          // DD-RAM address counter (cursor pos) to '0'
}


/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Low-level routine to write a byte to LCD-controller
 *
 * Writes one byte to the LCD-controller (by  calling LcdWriteNibble twice)
 * CtrlState determines if the byte is written to the instruction register
 * or to the data register.
 *
 * \param CtrlState destination: instruction or data
 * \param LcdByte byte to write
 *
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void LcdWriteByte(u_char CtrlState, u_char LcdByte)
{
    LcdWaitBusy();                      // see if the controller is ready to receive next byte
    LcdWriteNibble(CtrlState, LcdByte & 0xF0);
    LcdWriteNibble(CtrlState, LcdByte << 4);

}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Low-level routine to write a nibble to LCD-controller
 *
 * Writes a nibble to the LCD-controller (interface is a 4-bit databus, so
 * only 4 databits can be send at once).
 * The nibble to write is in the upper 4 bits of LcdNibble
 *
 * \param CtrlState destination: instruction or data
 * \param LcdNibble nibble to write (upper 4 bits in this byte
 *
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void LcdWriteNibble(u_char CtrlState, u_char LcdNibble)
{
    outp((inp(LCD_DATA_DDR) & 0x0F) | 0xF0, LCD_DATA_DDR);  // set data-port to output again

    outp((inp(LCD_DATA_PORT) & 0x0F) | (LcdNibble & 0xF0), LCD_DATA_PORT); // prepare databus with nibble to write

    if (CtrlState == WRITE_COMMAND)
    {
        cbi(LCD_RS_PORT, LCD_RS);     // command: RS low
    }
    else
    {
        sbi(LCD_RS_PORT, LCD_RS);     // data: RS high
    }

    sbi(LCD_EN_PORT, LCD_EN);

    asm("nop\n\tnop");                    // small delay

    cbi(LCD_EN_PORT, LCD_EN);
    cbi(LCD_RS_PORT, LCD_RS);
    outp((inp(LCD_DATA_DDR) & 0x0F), LCD_DATA_DDR);           // set upper 4-bits of data-port to input
    outp((inp(LCD_DATA_PORT) & 0x0F) | 0xF0, LCD_DATA_PORT);  // enable pull-ups in data-port
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Low-level routine to see if the controller is ready to receive
 *
 * This routine repeatetly reads the databus and checks if the highest bit (bit 7)
 * has become '0'. If a '0' is detected on bit 7 the function returns.
 *
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void LcdWaitBusy()
{
    u_char Busy = 1;
	u_char LcdStatus = 0;

    cbi (LCD_RS_PORT, LCD_RS);              // select instruction register

    sbi (LCD_RW_PORT, LCD_RW);              // we are going to read

    while (Busy)
    {
        sbi (LCD_EN_PORT, LCD_EN);          // set 'enable' to catch 'Ready'

        asm("nop\n\tnop");                  // small delay
        LcdStatus =  inp(LCD_IN_PORT);      // LcdStatus is used elsewhere in this module as well
        Busy = LcdStatus & 0x80;            // break out of while-loop cause we are ready (b7='0')
    }

    cbi (LCD_EN_PORT, LCD_EN);              // all ctrlpins low
    cbi (LCD_RS_PORT, LCD_RS);
    cbi (LCD_RW_PORT, LCD_RW);              // we are going to write
}

/* ---------- end of module ------------------------------------------------ */

/*@}*/
