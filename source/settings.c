#include <dev/board.h>
#include <stdio.h>
#include <io.h>
#include <dev/nvmem.h>

#include <string.h>
#include "settings.h"
#include "vs10xx.h"

static struct _SAVE_BLOCK file;
int reboot = 0;

//Save the struct to eeprom
void SaveSettings() {
	file.length = sizeof(file);
	NutNvMemSave(256,&file,sizeof(file));
}

//Loading file, and creating new one if none exists.
void LoadSettings() {
	NutNvMemLoad(256,&file,sizeof(file)); 
	
	if(file.length != sizeof(file)) {
		printf("[SETTINGS] No save data detected creating a new one...\n");
		FactoryReset();
	}
	else {
		printf("[SETTINGS] Save data detected, loading settings...\n");
		
		//Display debug data
		printf("[SETTINGS] FTS: %d, Timezone: %d, Backlight: %d, Snooze: %d, Sync: %d\n", file.firstTimeSetup, file.timeZone, file.backlight, file.snooze, file.sync);
		printf("[SETTINGS] Device: %c%c%c%c%c%c\n", file.dev0, file.dev1, file.dev2, file.dev3, file.dev4, file.dev5);
		printf("[SETTINGS] Volume: %d, Bass: %d, Treble: %d, Balance: %d\n", file.volume, file.bass, file.treble, file.balance);
	}
}

//Reboot bool
void Reboot(void)
{
	reboot = 1;
}

int GetReboot(void)
{
	return reboot;
}

//Reset all values to default
void FactoryReset(void)
{
	//Default settings
	file.timeZone = 0; //GMT;
	file.firstTimeSetup = 0; //Run FTS

	file.volume = 150; //medium Volume
	file.treble = 8;
	file.bass = 8;
	file.balance = 0;
	
	file.backlight = 0; //Turns off after timeout
	file.snooze = 15; //Snooze time
	file.sync = 1; //1 minute
	
	file.dev0 = 0;
	file.dev1 = 0;
	file.dev2 = 0;
	file.dev3 = 0;
	file.dev4 = 0;
	file.dev5 = 0;
	
	file.length = sizeof(file); //File length
	SaveSettings();
}

struct _SAVE_BLOCK *getSettingsFile() {
	return &file;
}

void SetFirstTimeSetup(int i)
{
	file.firstTimeSetup = i;
	SaveSettings();
}

int GetFirstTimeSetup( void )
{
	return file.firstTimeSetup;
}

void SetDeviceID(char * id)
{
	file.dev0 = id[0];
	file.dev1 = id[1];
	file.dev2 = id[2];
	file.dev3 = id[3];
	file.dev4 = id[4];
	file.dev5 = id[5];
	
	SaveSettings();
}

void GetDeviceID(char * id)
{
	id[0] = file.dev0;
	id[1] = file.dev1;
	id[2] = file.dev2;
	id[3] = file.dev3;
	id[4] = file.dev4;
	id[5] = file.dev5;
	id[6] = '\0';
}

void SetTimeZone(int i)
{
	file.timeZone = i;
	SaveSettings();
}

int GetTimeZone( void )
{
	return file.timeZone;
}

void SetVolume(int i)
{
	if(i > 254)
		i = 254;
	if(i < 0)
		i = 0;
	
	file.volume = i;
	
	VsSetVolume(i);
}
int GetVolume(void)
{
	return file.volume;
}
void SetTreble(int i)
{
	if(i > 0xF)
		i = 0xF;
	if(i < 0)
		i = 0;
	
	file.treble = i;
	VsSetTreble(i);
}
int GetTreble(void)
{
	return file.treble;
}
void SetBass(int i)
{
	if(i > 0xF)
		i = 0xF;
	if(i < 0)
		i = 0;
	
	file.bass = i;
	VsSetBass(i);
}
int GetBass(void)
{
	return file.bass;
}
void SetBalance(int i)
{
	if(i > 7)
		i = 7;
	if(i < -7)
		i = -7;
	
	file.balance = i;
	VsSetVolume(file.volume);
}
int GetBalance(void)
{
	return file.balance;
}


void SetBacklight(int i)
{
	file.backlight = i;
}

int GetBacklight(void)
{
	return file.backlight;
}

void SetSnooze(int i)
{
	file.snooze = i;
}

int GetSnooze(void)
{
	return file.snooze;
}

void SetSync(int i)
{
	file.sync = i;
}

int GetSync(void)
{
	return file.sync;
}
