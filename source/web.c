#include <sys/socket.h>
#include <pro/httpd.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "web.h"
#include "jsmn.h"
#include "streamer.h"
#include "alarm.h"
#include "scheduler.h"
#include "settings.h"


int getIntegerFromToken(const char *json, jsmntok_t *tok);
void getStringFromToken(const char *json, jsmntok_t *tok, char *res);
int jsoneq(const char *json, jsmntok_t *tok, const char *s);


/*
	Get the stream from a given ip and port.
	IP - The ip adress of the TCP server.
	PORT - the port of the TCP server.
	Return - the stream tot the server.
*/
FILE *getStream(TCPSOCKET * sock, u_long ip, u_short port) {
	FILE *stream;
    u_long rx_to = 750;
	
	if ((sock = NutTcpCreateSocket()) == 0) 
		printf("[WEB] Probleem bij het creereen van tcp socket\n");
	if (NutTcpSetSockOpt(sock, SO_RCVTIMEO, &rx_to, sizeof(rx_to)))
		printf("[WEB] Probleem bij het creereen van tcp socket\n");
	
    if ((NutTcpConnect(sock, ip, port))) {
        printf("[WEB] Error: Connect failed\n");
        return 0;
    }
	if ((stream = _fdopen((int) sock, "r+b")) == 0) {
        printf("[WEB] Error: Can't create stream\n");
        return 0;
    }
	else
		printf("[WEB] Stream connected succesfully to ip: %s\n",inet_ntoa(ip));
	return stream; //succesfully connected.
}

/*
	Get the JSON from an given stream on a given path.
	Stream - The stream to the server.
	Path - The path to the JSON file on the server.
	Return - The JSON string as a char array.
*/
void getJSON(FILE * stream, char * path, char * line) {
	//char *line = malloc(MAX_HEADERLINE); //Recieved HTTP line;
	
    char *cp;
	//Sending get request:
	fprintf(stream, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",path,"lipa.kvewijk.nl");
    fflush(stream);
	
	//Removing header:
    while(fgets(line, MAX_HEADERLINE, stream)) {
        cp = strchr(line, '\r');
		if(cp == 0) 
            continue;
        *cp = 0;
        if(*line == 0) 
            break;
        //printf("Header: %s\n", line);
    }   
	
	//Finding body & JSON:
    while(fgets(line, MAX_HEADERLINE, stream)) {
		if(line[0] == '{') 
			break;
    } 
}

//******************************************************
// Parses the given alarm json string to usefull alarms.
// Parameter json - The json string containing the alarms.
//******************************************************
void parseAlarmJSON(char *json) {
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmntok_t * tokens = (jsmntok_t*) calloc(MAXTOKENS,sizeof(jsmntok_t));

	int size = jsmn_parse(&parser, json, strlen(json), tokens,MAXTOKENS);

	int x;
	char name[32];
	char ip[32];
	char path[64];

	int id = 0;
	int hour = 0;
	int minute = 0;
	int port = 0;

	for(x = 0; x < size; x++) {
		if (tokens[x].type == JSMN_STRING) {
			if (jsoneq(json, &tokens[x], "id") == 0) {
				id = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("ID: %d\n", id);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "name") == 0) {
				getStringFromToken(json, &tokens[x + 1], name);
				//printf("Name: %s\n", name);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "hour") == 0) {
				hour = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Hour: %d\n", hour);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "minute") == 0) {
				minute = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Minute: %d\n", minute);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "ip") == 0) {
				getStringFromToken(json, &tokens[x + 1], ip);
				//printf("IP: %s\n", ip);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "port") == 0) {
				port = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Port: %d\n", port);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "path") == 0) {
				getStringFromToken(json, &tokens[x + 1], path);
				//printf("Path: %s\n", path);
				createAlarm(id, name, hour, minute, inet_addr(ip), port, (u_char *) path);
				x++;
			}
		}
	}
	free(tokens);
}

char * alarmJSON;
//********************************
//Get the alarms from the server.
//********************************
int getAlarms() {
	
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	TCPSOCKET * sock = { 0 };
	char path[30];
	sprintf(path,"/api/alarms.php?id=%s",deviceID);
	u_long alarm_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, alarm_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	alarmJSON = calloc(LARGEALLOC, sizeof(char));
	
	if(alarmJSON == NULL)
	{
		free(alarmJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,alarmJSON);
	parseAlarmJSON(alarmJSON);
	
	free(alarmJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

//**********************************************************
// Get the string from a given token.
// json - The json string to get the token from.
// tok - The token that you want to get out of the json.
// res - The string you want to aquire gets placed in here.
//**********************************************************
void getStringFromToken(const char *json, jsmntok_t *tok, char *res) {
	int index = 0;
	int y;
	for(y = tok->start; y < tok->end; y++) {
		res[index++] = json[y];
	} 
	res[index] = '\0';
}

//********************************************************
// Get the integer value from a given token.
// json - The json string to get the token from.
// tok - The token that you want to get out of the json.
//********************************************************
int getIntegerFromToken(const char *json, jsmntok_t *tok) {
	char integer[(tok->end-tok->start)+1];
	getStringFromToken(json,tok,integer);
	return atoi(integer);
}

//********************************************
// Find a token in a json string.
// json - The json to get the token from.
// tok - The token to compare to.
// s - The string value of the token.
//********************************************
int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

char * registerJSON;
// ****************************************
// Register Device on the server.
// Return 1 if succesfull and 0 if failed.
//********************************
int registerDevice() {
	
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	if(deviceID[0] != 0)
		return 1;
	
	TCPSOCKET * sock = { 0 };
	char path[30];
	sprintf(path,"/api/register.php");
	u_long register_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, register_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	registerJSON = calloc(SMALLALLOC, sizeof(char));
	
	if(registerJSON == NULL)
	{
		free(registerJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,registerJSON);
	parseRegisterJSON(registerJSON);
	
	free(registerJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

void parseRegisterJSON(char * json)
{
	//Parsing JSON:
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmntok_t tokens[20];

	int size = jsmn_parse(&parser, json, strlen(json), tokens, sizeof(tokens)/sizeof(tokens[0]));

	int x;
	char id[6];
	
	for(x = 0; x < size; x++) {
		if (tokens[x].type == JSMN_STRING) {
			if (jsoneq(json, &tokens[x], "device") == 0) {
				getStringFromToken(json,&tokens[x+1],id);
				//printf("Device id: %s\n",id);
				x++;
			}
		}
	}
	SetDeviceID(id);
}

char * timeZoneJSON;
// *********************************************************
// Get timezone from the server.
// Return array index of the timezone, if failed return -1.
// *********************************************************
int getTimezoneOffset(int * timezone) {
	
	TCPSOCKET * sock = { 0 };
	char path[30];
	sprintf(path,"/api/timezone.php");
	u_long register_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, register_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	timeZoneJSON = calloc(SMALLALLOC, sizeof(char));
	
	if(timeZoneJSON == NULL)
	{
		free(timeZoneJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,timeZoneJSON);
	parseTimeZoneJSON(timeZoneJSON, timezone);
	
	free(timeZoneJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

void parseTimeZoneJSON(char * json, int * timezone)
{
	//Parsing JSON:
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmntok_t tokens[10];

	int size = jsmn_parse(&parser, timeZoneJSON, strlen(timeZoneJSON), tokens, sizeof(tokens)/sizeof(tokens[0]));
	
	int x;
	for(x = 0; x < size; x++) {
		if (tokens[x].type == JSMN_STRING) {
			if (jsoneq(timeZoneJSON, &tokens[x], "timezone") == 0) 
				*timezone = getIntegerFromToken(timeZoneJSON,&tokens[x+1]);
		}
	}
}

char * resetJSON;
//*******************************************
// Resets the device settings on the server.
//*******************************************
int resetServer() {
	
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	TCPSOCKET * sock = { 0 };
	char path[45];
	sprintf(path,"/api/reset.php?id=%s&key=boef1239",deviceID);
	u_long reset_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, reset_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	resetJSON = calloc(SMALLALLOC, sizeof(char));
	
	if(resetJSON == NULL)
	{
		free(resetJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,resetJSON);
	
	free(resetJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

char * streamJSON;
//********************************
//Get the alarms from the server.
//********************************
int getStreams() {
	
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	TCPSOCKET * sock = { 0 };
	char path[30];
	sprintf(path,"/api/streams.php?id=%s",deviceID);
	u_long stream_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, stream_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	streamJSON = calloc(LARGEALLOC, sizeof(char));
	
	if(streamJSON == NULL)
	{
		free(streamJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,streamJSON);
	parseStreamJSON(streamJSON);
	
	free(streamJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

//******************************************************
// Parses the given stream json string to usefull streams.
// Parameter json - The json string containing the streams.
//******************************************************
void parseStreamJSON(char *json) {
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmntok_t * tokens = (jsmntok_t*) calloc(MAXTOKENS,sizeof(jsmntok_t));

	int size = jsmn_parse(&parser, json, strlen(json), tokens, MAXTOKENS);

	char name[32];
	char ip[32];
	char path[32];
	int port = 80;
	int id = 0;
	int x;

	for(x = 0; x < size; x++) {
		if (tokens[x].type == JSMN_STRING) {
			if (jsoneq(json, &tokens[x], "id") == 0) {
				id = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("ID: %d\n", id);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "name") == 0) {
				getStringFromToken(json, &tokens[x + 1], name);
				//printf("Name: %s\n", name);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "ip") == 0) {
				getStringFromToken(json, &tokens[x + 1], ip);
				//printf("IP: %s\n", ip);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "port") == 0) {
				port = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Port: %d\n", port);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "path") == 0) {
				getStringFromToken(json, &tokens[x + 1], path);
				//printf("Path: %s\n", path);
				x++;
				createStream(id, name, inet_addr(ip), port, (u_char*) path);
			}
		}
	}
	free(tokens);
}

char * tweetJSON;

int tweet(char * body)
{
	//put deviceID in a char variable
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	//Open stream
	TCPSOCKET * sock = { 0 };
	u_long stream_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, stream_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	//Allocate memory for tweetJSON
	tweetJSON = calloc(LARGEALLOC, sizeof(char));
	
	if(tweetJSON == NULL)
	{
		free(tweetJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	//set JSON message and send it to the server
	char path[200];
	sprintf(path,"/api/twitter.php?body=%s&id=%s",body,deviceID);
	getJSON(stream,path,tweetJSON);
	
	free(tweetJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

char * scheduleJSON;
//********************************
//Get the alarms from the server.
//********************************
int getSchedules() {
	
	char deviceID[7] = {0};
	GetDeviceID(deviceID);
	
	TCPSOCKET * sock = { 0 };
	char path[30];
	sprintf(path,"/api/schedules.php?id=%s",deviceID);
	u_long stream_ip = inet_addr("31.22.4.228");
	FILE *stream = getStream(sock, stream_ip, 80);
	
	if(stream == 0)
	{
		printf("[WEB] Stream failed\n");
		return 0;
	}
	
	scheduleJSON = calloc(LARGEALLOC, sizeof(char));
	
	if(scheduleJSON == NULL)
	{
		free(scheduleJSON);
		printf("[WEB] Alloc failed\n");
		return 0;
	}
	
	getJSON(stream,path,scheduleJSON);
	parseScheduleJSON(scheduleJSON);
	
	free(scheduleJSON);
	
	fclose(stream);
	NutTcpCloseSocket(sock);
	
	return 1;
}

//******************************************************
// Parses the given stream json string to usefull streams.
// Parameter json - The json string containing the streams.
//******************************************************
void parseScheduleJSON(char *json) {
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmntok_t * tokens = (jsmntok_t*) calloc(MAXTOKENS,sizeof(jsmntok_t));

	int size = jsmn_parse(&parser, json, strlen(json), tokens, MAXTOKENS);

	char name[32];
	char ip[32];
	char path[32];
	int hour = 0;
	int min = 0;
	int dhour = 0;
	int dmin = 0;
	int port = 80;
	int id = 0;
	int x;

	for(x = 0; x < size; x++) {
		if (tokens[x].type == JSMN_STRING) {
			if (jsoneq(json, &tokens[x], "id") == 0) {
				id = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("ID: %d\n", id);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "name") == 0) {
				getStringFromToken(json, &tokens[x + 1], name);
				//printf("Name: %s\n", name);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "hour") == 0) {
				hour = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Hour: %d\n", hour);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "minute") == 0) {
				min = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Minute: %d\n", min);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "dhour") == 0) {
				dhour = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Hour: %d\n", dhour);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "dminute") == 0) {
				dmin = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Minute: %d\n", dmin);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "ip") == 0) {
				getStringFromToken(json, &tokens[x + 1], ip);
				//printf("IP: %s\n", ip);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "port") == 0) {
				port = getIntegerFromToken(json, &tokens[x + 1]);
				//printf("Port: %d\n", port);
				x++;
			}
			else if (jsoneq(json, &tokens[x], "path") == 0) {
				getStringFromToken(json, &tokens[x + 1], path);
				//printf("Path: %s\n", path);
				createSchedule(id, name, hour, min, dhour, dmin, inet_addr(ip), port, (u_char*) path);
				x++;
			}
		}
	}
	free(tokens);
}
