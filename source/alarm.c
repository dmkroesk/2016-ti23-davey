#include <dev/board.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <dev/nvmem.h>
#include <arpa/inet.h>
#include <sys/timer.h>
#include <sys/thread.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <string.h>

#include "time.h"
#include "alarm.h"
#include "rtc.h"
#include "player.h"
#include "web.h"
#include "menu.h"
#include "settings.h"

ALARM_STRUCT *alarms; //Array of alarms

int * ringingalarm;	//Number of alarm that is ringing
int * alarmringing;	//Bool if a alarm is ringing

//Get the name of the alarm currently going off
void GetCurrentAlarmName(char * str)
{
	strcpy(str, alarms[*ringingalarm].name);
}

//Get if an alarm is ringing
int AlarmRinging(void)
{
	return *alarmringing;
}

//Stop the current ringing alarm
void StopAlarm(void)
{
	*alarmringing = 0;
	alarms[*ringingalarm].snooze = 0;
	*ringingalarm = -1;
	stop();
}

//Snooze the current ringing alarm
void SnoozeAlarm(void)
{
	*alarmringing = 0;
	alarms[*ringingalarm].snooze = alarms[*ringingalarm].snooze + 1 > 3 ? 0 : alarms[*ringingalarm].snooze + 1;
	*ringingalarm = -1;
	stop();
}

//alarm is the alarm to be compared with the currentTime
//currentTime is the currentTime, also set in the rtc chip
int compareAlarmTime(ALARM_STRUCT alarm, tm currentTime)
{				
	int min = alarm.min;
	int hour = alarm.hour;
	
	//Calculate the snooze time
	if(alarm.snooze > 0)
	{
		min += (alarm.snooze*GetSnooze());
		hour += min / 60;
		min = min % 60;
	}
				
	printf("[ALARM] Comparing [%d:%d] ", currentTime.tm_hour, currentTime.tm_min);
	printf("to alarm [%d:%d] with snooze: %d and ringing: %d\n", hour, min, alarm.snooze, *alarmringing);			
				
	if(hour == currentTime.tm_hour){	//compare the hour
		
		//check if alarm should ring
		if(min == currentTime.tm_min && *alarmringing == 0){
			return 1; //time is same as alarm
		}
		
		//check if alarm should stop
		else if(currentTime.tm_min - 2 > min && *alarmringing == 1)
		{
			return 2; //alarm is ringing to long
		}
	}
	
	return 0;//time is not same alarm
}

char tweetJSON[512];

//Send a tweet about a timestamp
void tweetAlarm(int hour, int min, int sec)
{
	char str[141];
	
	sprintf(str, "Alarm%%20at%%20%02d:%02d:%02d", hour, min, sec);
	tweet(str);
}

//Create a alarm from all the needed parameters and add it to the list.
void createAlarm(int id, char* name, int hour, int min, u_long ip, u_short port, u_char *path)
{
	ALARM_STRUCT * alarm = (ALARM_STRUCT *) calloc(1, sizeof(ALARM_STRUCT));
	
	alarm->valid = 1;
	alarm-> on = 1;
	alarm->id = id;
	alarm->updated = 1;
	
	strncpy(alarm->name, name, sizeof(alarm->name));
	alarm->hour = hour;
	alarm->min = min;
	
	alarm->ip = ip;
	alarm->port = port;
	strncpy((char*) alarm->path, (char*) path, sizeof(alarm->path));
	
	alarm->snooze = 0;
	
	addAlarm(alarm);
}

//Clear all the old alarms that are not new or updated
void clearAlarms()
{
	int i;
	
	for(i = 0; i<MAX_ALARMS; i++)
	{
		if(alarms[i].updated == 0) //Alarm that has not been updated (not on the website anymore)
		{
			printf("[ALARM] Removing old alarm %d with id %d\n", i, alarms[i].id);
			memset(&alarms[i], 0, sizeof(alarms[i]));
		}
		else	//Keep the alarm and remove the updated tag
		{
			printf("[ALARM] Keeping alarm %d with id %d\n", i, alarms[i].id);
			alarms[i].updated = 0;
		}
	}
}

//Try to add a alarm to the alarms array
int addAlarm(ALARM_STRUCT * alarm)
{
	int i;

	for(i = 0; i<MAX_ALARMS; i++)
	{
		//Alarm is already known and will be updated
		if(alarms[i].valid == 1 && alarms[i].id == alarm->id)
		{
			printf("[ALARM] Updated alarm at pos %d\n", i);
			alarms[i].updated = 1;
			alarms[i].hour = alarm->hour;
			alarms[i].min = alarm->min;
			strncpy(alarms[i].name, alarm->name, sizeof(alarms[i].name));
			alarms[i].ip = alarm->ip;
			alarms[i].port = alarm->port;
			strncpy((char*) alarms[i].path, (char*) alarm->path, sizeof(alarms[i].path));
			break;
		}
		//Alarm is new and there is place in the array
		else if(alarms[i].valid != 1)
		{
			printf("[ALARM] Adding new alarm to pos %d\n", i);
			alarms[i] = *alarm;
			break;
		}
	}
	
	free(alarm);
	
	return 0;
}

//Get an alarm 
ALARM_STRUCT GetAlarm(int i)
{
	if(i > MAX_ALARMS - 1)
	{
		i = MAX_ALARMS - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	return alarms[i];
}

//Check if an alarm should ring or stop etc.
void checkAlarms(void)
{
	int i;
	int p;
	
	for(i = 0; i<MAX_ALARMS; i++)
	{
		if(alarms[i].valid == 1 && alarms[i].on == 1)	//Actual alarm in array
		{
			p = compareAlarmTime(alarms[i], *GetTimeStruct());	//Check alarm against current time
			
			if(p == 1)	//Alarm should ring
			{
				printf("[ALARM] Alarm %d:%s is ringing", i, alarms[i].name);
				
				//Start alarm
				*alarmringing = 1;
				*ringingalarm = i;
				
				//Tweet the alarm
				tweetAlarm(alarms[i].hour, alarms[i].min, 0);
				
				NutSleep(10);
				
				//Update the menu to show the alarm
				AlarmStartedRinging();
				
				//Start streaming the audio
				play(alarms[i].ip, alarms[i].port, alarms[i].path);
			}
			else if(p == 2)	//Alarm should stop ringing
			{
				printf("[ALARM] Alarm %d:%s has timed out", i, alarms[i].name);
				//Auto snooze the alarm at timeout
				SnoozeAlarm();
				//Navigate menu back to home
				AlarmStoppedRinging();
			}
		}
	}
}

void ToggleAlarmOn(int i)
{
	if(i > MAX_ALARMS - 1)
	{
		i = MAX_ALARMS - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	if(alarms[i].on == 1)
	{
		alarms[i].on = 0;
	}
	else
	{
		alarms[i].on = 1;
	}
}

//Get the alarm name
void GetAlarmName(int i, char * str)
{
	if(i > MAX_ALARMS - 1)
	{
		i = MAX_ALARMS - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	strcpy(str, alarms[i].name);
}

//Get the alarm time
void GetAlarmTime(int i, char* str)
{
	if(i > MAX_ALARMS - 1)
	{
		i = MAX_ALARMS - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	sprintf(str, "%02d:%02d", alarms[i].hour, alarms[i].min);
}

//Get the total amount of valid alarms
int GetNumAlarms(void)
{
	int count = 0;
	int i;
	for(i=0; i< MAX_ALARMS; i++)
	{
		if(alarms[i].valid == 1)
			count++;
	}
	
	return count;
}

int GetAlarmOn(int i)
{
	if(i > MAX_ALARMS - 1)
	{
		i = MAX_ALARMS - 1;
	}
	if(i < 0)
	{
		i = 0;
	}
	
	return alarms[i].on;
}

int AlarmsOn(void)
{
	int on = 0;
	int i;
	
	for(i=0; i< MAX_ALARMS; i++)
	{
		if(alarms[i].valid == 1 && alarms[i].on == 1)
		{
			on = 1;
			break;
		}
	}
	
	return on;
}

//Allocate the memory for the array
void InitAlarms( void )
{
	alarmringing = calloc(1, sizeof(int));
	ringingalarm = calloc(1, sizeof(int));
	
	*alarmringing = 0;
	*ringingalarm = -1;
	
	alarms = calloc(MAX_ALARMS, sizeof(ALARM_STRUCT));
}
