#include "network.h"

#include <stdlib.h>
#include <string.h>
#include <io.h>
#include <fcntl.h>

#include <dev/board.h>
#include <dev/urom.h>
#include <dev/nicrtl.h>
#include <dev/nplmmc.h>
#include <dev/sbimmc.h>
#include <fs/phatfs.h>

#include <sys/version.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/heap.h>
#include <sys/confnet.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <net/route.h>
#include <netinet/tcp.h>

#include <pro/httpd.h>
#include <pro/dhcp.h>
#include <pro/ssi.h>
#include <pro/asp.h>
#include <pro/sntp.h>
#include <pro/discover.h>

#include "system.h"
#include "log.h"
int connected = 0;

int GetConnected(void)
{
	return connected;
}

void SetConnected(int i)
{
	connected = i;
}

void networkInitialize()
{
    // Initialize network here.
    u_long baud = 115200;

    NutRegisterDevice(&DEV_DEBUG, 0, 0);
    freopen(DEV_DEBUG_NAME, "w", stdout);
    _ioctl(_fileno(stdout), UART_SETSPEED, &baud);
    NutSleep(200);
    printf("[NETWORK] IPAC Network Daemon...\n");

    if(NutRegisterDevice(&DEV_ETHER, 0,0))
    {
        printf("[NETWORK] Registering device failed\n");
		return;
    }

    printf("[NETWORK] Configuring %s...\n", DEV_ETHER_NAME);

    if(NutDhcpIfConfig(DEV_ETHER_NAME, MY_MAC, 60000))
    {
		return;
    }

    printf("[NETWORK] Network configured using DHCP\n"); 

    NutRegisterDiscovery((u_long)-1, 0, DISF_INITAL_ANN); 
}
